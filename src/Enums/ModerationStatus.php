<?php
namespace mathewparet\RequiresModeration\Enums;

enum ModerationStatus: String
{
    case PENDING = 'PENDING';

    case FLAGGED = 'FLAGGED';

    case REJECTED = 'REJECTED';

    case APPROVED = 'APPROVED';
}