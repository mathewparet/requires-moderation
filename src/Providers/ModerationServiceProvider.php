<?php
namespace mathewparet\RequiresModeration\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use mathewparet\RequiresModeration\Enums\ModerationStatus;

class ModerationServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->registerBlueprintMacros();
    }

    public function register(): void
    {
        // 
    }

    private function registerBlueprintMacros()
    {
        Blueprint::macro("requiresModeration", function($name = 'status') {
            /**
             * @var \Illuminate\Database\Schema\Blueprint $this
             */
            return $this->string($name)->default(ModerationStatus::PENDING->value);
        });
    }
}