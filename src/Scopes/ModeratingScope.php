<?php
namespace mathewparet\RequiresModeration\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use mathewparet\RequiresModeration\Enums\ModerationStatus;

class ModeratingScope implements Scope
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var string[]
     */
    protected $extensions = ['Approve', 'Reject', 'Flag', 'WithPending', 'WithoutPending', 'OnlyPending', 'OnlyFlagged', 'OnlyRejected'];

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereNot($model->getQualifiedModerationStatusColumn(), ModerationStatus::APPROVED);
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Get the "published at" column for the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return string
     */
    protected function getModerationStatusColumn(Builder $builder)
    {
        if (count((array) $builder->getQuery()->joins) > 0) {
            return $builder->getModel()->getQualifiedModerationStatusColumn();
        }

        return $builder->getModel()->getModerationStatusColumn();
    }

    /**
     * Add the approve extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addApprove(Builder $builder)
    {
        $builder->macro('approve', function (Builder $builder) {
            $builder->withPending();

            return $builder->update([$builder->getModel()->getModerationStatusColumn() => ModerationStatus::APPROVED]);
        });
    }
    
    /**
     * Add the flag extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addFlag(Builder $builder)
    {
        $builder->macro('flag', function (Builder $builder) {
            $builder->withPending();

            return $builder->update([$builder->getModel()->getModerationStatusColumn() => ModerationStatus::FLAGGED]);
        });
    }
    
    /**
     * Add the reject extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addReject(Builder $builder)
    {
        $builder->macro('reject', function (Builder $builder) {
            $builder->withPending()->withFlagged();

            return $builder->update([$builder->getModel()->getModerationStatusColumn() => ModerationStatus::REJECTED]);
        });
    }

    /**
     * Add the with-pending extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithPending(Builder $builder)
    {
        $builder->macro('withPending', function (Builder $builder) {
            $model = $builder->getModel();

            return $builder->withoutGlobalScope($this);
        });
    }

    /**
     * Add the without-pending extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithoutPending(Builder $builder)
    {
        $builder->macro('withoutPending', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNot(
                $model->getQualifiedModerationStatusColumn(),
                ModerationStatus::PENDING
            );

            return $builder;
        });
    }

    /**
     * Add the only-pending extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyPending(Builder $builder)
    {
        $builder->macro('onlyPending', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->where(
                $model->getQualifiedModerationStatusColumn(),
                ModerationStatus::PENDING
            );

            return $builder;
        });
    }

    /**
     * Add the only-flagged extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyFlagged(Builder $builder)
    {
        $builder->macro('onlyFlagged', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->where(
                $model->getQualifiedModerationStatusColumn(),
                ModerationStatus::FLAGGED
            );

            return $builder;
        });
    }
    
    /**
     * Add the only-rejected extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyRejected(Builder $builder)
    {
        $builder->macro('onlyRejected', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->where(
                $model->getQualifiedModerationStatusColumn(),
                ModerationStatus::REJECTED
            );

            return $builder;
        });
    }
    
}