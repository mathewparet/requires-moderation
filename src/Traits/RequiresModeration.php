<?php
namespace mathewparet\RequiresModeration\Traits;

use mathewparet\RequiresModeration\Enums\ModerationStatus;
use mathewparet\RequiresModeration\Scopes\ModeratingScope;

trait RequiresModeration
{
    /**
     * Boot the RequiresModeration trait for a model.
     *
     * @return void
     */
    public static function bootRequiresModeration()
    {
        static::addGlobalScope(new ModeratingScope);
    }

    /**
     * Initialize the RequiresModeration trait for an instance.
     *
     * @return void
     */
    public function initializeRequiresModeration()
    {
        if (! isset($this->casts[$this->getModerationStatusColumn()])) {
            $this->casts[$this->getModerationStatusColumn()] = ModerationStatus::class;
        }
    }

    /**
     * Approve a model
     *
     * @return bool|null
     */
    public function approve()
    {
        if ($this->fireModelEvent('approving') === false) {
            return false;
        }
        return tap($this->runApproving(), function ($approved) {
            if ($approved) {
                $this->fireModelEvent('approved', false);
            }
        });
    }

    /**
     * Perform the actual approvel query on this model instance.
     *
     * @return mixed
     */
    protected function performApproveOnModel()
    {
        return $this->runApproving();
    }

    /**
     * Perform the actual approval query on this model instance.
     *
     * @return void
     */
    protected function runApproving()
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $time = $this->freshTimestamp();

        $columns = [$this->getModerationStatusColumn() => ModerationStatus::APPROVED->value];

        $this->{$this->getModerationStatusColumn()} = ModerationStatus::APPROVED->value;

        if ($this->usesTimestamps() && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $query->update($columns);

        $this->syncOriginalAttributes(array_keys($columns));

        $this->fireModelEvent('approved', false);
    }

    /**
     * Approve model without raising any events.
     *
     * @return bool|null
     */
    public function approveQueitly()
    {
        return static::withoutEvents(fn () => $this->runApproving());
    }

    /**
     * Register a "approving" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function approving($callback)
    {
        static::registerModelEvent('approving', $callback);
    }

    /**
     * Register a "approved" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function approved($callback)
    {
        static::registerModelEvent('approved', $callback);
    }

    /**
     * Determine if the model instance has been published.
     *
     * @return bool
     */
    public function isApproved()
    {
        return $this->{$this->getModerationStatusColumn()} == ModerationStatus::APPROVED;
    }
    
    /**
     * Determine if the model instance has been published.
     *
     * @return bool
     */
    public function isFlagged()
    {
        return $this->{$this->getModerationStatusColumn()} == ModerationStatus::FLAGGED;
    }
    
    /**
     * Determine if the model instance has been published.
     *
     * @return bool
     */
    public function isRejected()
    {
        return $this->{$this->getModerationStatusColumn()} == ModerationStatus::REJECTED;
    }
    
    /**
     * Determine if the model instance has been published.
     *
     * @return bool
     */
    public function isPending()
    {
        return $this->{$this->getModerationStatusColumn()} == ModerationStatus::PENDING;
    }

    /**
     * Reject model instance.
     *
     * @return bool
     */
    public function reject()
    {
        if ($this->fireModelEvent('rejecting') === false) {
            return false;
        }

        $this->{$this->getModerationStatusColumn()} = ModerationStatus::REJECTED;

        $result = $this->save();

        $this->fireModelEvent('rejected', false);

        return $result;
    }

    /**
     * Reject model instance without raising any events.
     *
     * @return bool
     */
    public function rejectQuietly()
    {
        return static::withoutEvents(fn () => $this->reject());
    }

    /**
     * Register a "rejecting" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function rejecting($callback)
    {
        static::registerModelEvent('rejecting', $callback);
    }

    /**
     * Register a "rejected" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function rejected($callback)
    {
        static::registerModelEvent('rejected', $callback);
    }
    
    /**
     * Flag model instance.
     *
     * @return bool
     */
    public function flag()
    {
        if ($this->fireModelEvent('flagging') === false) {
            return false;
        }

        $this->{$this->getModerationStatusColumn()} = ModerationStatus::FLAGGED;

        $result = $this->save();

        $this->fireModelEvent('flagged', false);

        return $result;
    }

    /**
     * Flag model instance without raising any events.
     *
     * @return bool
     */
    public function flagQuietly()
    {
        return static::withoutEvents(fn () => $this->flag());
    }

    /**
     * Register a "flagging" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function flagging($callback)
    {
        static::registerModelEvent('flagging', $callback);
    }

    /**
     * Register a "flagged" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function flagged($callback)
    {
        static::registerModelEvent('flagged', $callback);
    }

    /**
     * Get the name of the "published at" column.
     *
     * @return string
     */
    public function getModerationStatusColumn()
    {
        return defined(static::class.'::MODERATION_STATUS') ? static::MODERATION_STATUS : 'status';
    }

    /**
     * Get the fully qualified "published at" column.
     *
     * @return string
     */
    public function getQualifiedModerationStatusColumn()
    {
        return $this->qualifyColumn($this->getModerationStatusColumn());
    }
}